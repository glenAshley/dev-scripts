# Development scripts

## Install

Include latest tag or hash for `vX.X.X`

```
yarn add -D gitlab:glenashley/dev-scripts#[vX.X.X]
```

## config

dev-scripts adds the following config files to the project root

```
.eslintrc.json
.flowconfig
.huskyrc.json
.importsortrc.json
.nvmrc
.prettierrc.json
```
